/*
  * core: https://github.com/stm32duino/Arduino_Core_STM32
  * bootloader: https://github.com/Serasidis/STM32_HID_Bootloader (hid_generic_pc13.bin)
  * Board: Generic STM32F1 series
  *  part: BluePill F103C8
  *  UART: enabled, no generic
  *   USB: CDC, generic
  *        low speed
  *  Opt.: -Os
  *  libc: Newlib Nano
  *  Upl.: HID 2.2

*/

#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

#define LED_PIN PC15
#define WIDTH 8
#define HEIGHT 8

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(WIDTH, HEIGHT, LED_PIN,
  NEO_MATRIX_BOTTOM  + NEO_MATRIX_RIGHT +
  NEO_MATRIX_ROWS    + NEO_MATRIX_PROGRESSIVE,
  NEO_GRB            + NEO_KHZ800);

void setup() {
  matrix.begin();
  matrix.fillScreen(matrix.Color(255,0,0));
  matrix.setBrightness(20);
  matrix.show();
  Serial.begin();
  matrix.fillScreen(matrix.Color(0,0,0));
  matrix.show();
}

void setPixel(const char* args){ // P <x> <y> <r> <g> <b>
  int x,y;
  int r,g,b;
  sscanf(args, "%d %d %d %d %d",&x,&y,&r,&g,&b);
  uint32_t color = Adafruit_NeoPixel::Color(r,g,b);
  matrix.setPassThruColor(color);
  matrix.drawPixel(x,y,0);  
  matrix.show();
}

void drawLine(const char* args){ // L {H|V} <fixed_coord> <start_coord> <end_coord> <r> <g> <b>
  int at, start, end,r,g,b;
  char dir;
  sscanf(args, "%c %d %d %d %d %d %d", &dir, &at, &start, &end, &r, &g, &b);
  uint32_t color = Adafruit_NeoPixel::Color(r,g,b);
  matrix.setPassThruColor(color);
  for(int i=start; i<=end; i++){
    if(dir=='H')
      matrix.drawPixel(i,at,0);
    else
      matrix.drawPixel(at,i,0);
  }
  matrix.show();
}

void fillScreen(const char* args){ // F <r> <g> <b>
  int r,g,b;
  sscanf(args, "%d %d %d",&r,&g,&b);
  uint32_t color = Adafruit_NeoPixel::Color(r,g,b);
  matrix.setPassThruColor(color);
  matrix.fillScreen(0);  
  matrix.show();
}

void setBrightness(const char* args){ // B <brightness>
  int b;
  sscanf(args, "%d",&b);
  matrix.setBrightness(b);
  matrix.show();
}

uint32_t vis_color = Adafruit_NeoPixel::Color(0,0x50,0xff);
uint32_t vis_half_color = Adafruit_NeoPixel::Color(0,0x50,0);
uint32_t vis_bg_color = Adafruit_NeoPixel::Color(0x10,0,0);

void setVisCol(const char* args){ // V <R> <G> <B> <r> <g> <b> <_r> <_g> <_b>
  int r,g,b,R,G,B,_r,_g,_b;       //    full color  half color   bgcolor
  sscanf(args, "%d %d %d %d %d %d %d %d %d",&R,&G,&B,&r,&g,&b, &_r, &_g, &_b);
  vis_color = Adafruit_NeoPixel::Color(R,G,B);
  vis_half_color = Adafruit_NeoPixel::Color(r,g,b);
  vis_bg_color = Adafruit_NeoPixel::Color(_r,_g,_b);
}

void visBars(const char* args){ // v <bar1> <bar2> ... <barN>
  int bars[WIDTH];
  for(int i=0;i<WIDTH;i++){
    int num;
    sscanf(args, "%d %n",&bars[i],&num);
    args=args+(num*sizeof(char));
  }
  matrix.setPassThruColor(vis_bg_color);
  matrix.fillScreen(0);
  matrix.setPassThruColor(vis_color);
  for(int x=0;x<WIDTH;x++){
    int up = floor((bars[x]+1)/2.0);
    int down = floor(bars[x]/2.0);
    for(int y=HEIGHT-up;y<HEIGHT;y++){
      if(up>down && y==HEIGHT-up)
        matrix.setPassThruColor(vis_half_color);
      matrix.drawPixel(x,y,0);
      if(up>down && y==HEIGHT-up)
        matrix.setPassThruColor(vis_color);
    }
  }
  matrix.show();
}

void loop() {
  if(Serial.available()<=0)
    return;
  String line = Serial.readStringUntil('\n');
  line.trim();
  char cmd = line.charAt(0);
  line = line.substring(2);
  const char* args = line.c_str();
  switch(cmd){
    case 'P':
      setPixel(args);
      break;
    case 'F':
      fillScreen(args);
      break;
    case 'L':
      drawLine(args);
      break;
    case 'B':
      setBrightness(args);
      break;
    case 'V':
      setVisCol(args);
      break;
    case 'v':
      visBars(args);
      break;
  }
}
