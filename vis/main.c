#include "Impulse.h"
#include <stdio.h>
#include <math.h>

#define NUM_BARS 8
#define BAR_HEIGHT 16

#define ARRSIZE 256

#define SCALE_X 4
#define SCALE_Y 0.33

#define INCREMENT (ARRSIZE/(NUM_BARS*SCALE_X))

int main() {

  im_start();

  double midpoint = BAR_HEIGHT/2;

  while ( 1 ) {
    usleep( 1000000 / 30 );
    double *array = im_getSnapshot(IM_FFT);
    int i;
    printf("v");
    double line_avg=0;
    int line_num=0;
    for (i = 0; i < ARRSIZE/SCALE_X; i+=INCREMENT){
      double val = (log1p(array[i]) + log1p(array[i+INCREMENT/2]))/2;
      line_avg+=val; line_num++;
      double scaled = val/midpoint * (BAR_HEIGHT*SCALE_Y);
      int clamped = floor(scaled);
      if(clamped<0)
        clamped=0;
      if(clamped>BAR_HEIGHT)
        clamped=BAR_HEIGHT;
      printf(" %d", clamped);
    }
    midpoint = midpoint * 0.5 + (line_avg/line_num)*0.5;
    printf("\n");
    fflush(stdout);
  }
  im_stop();

  return 0;
}
